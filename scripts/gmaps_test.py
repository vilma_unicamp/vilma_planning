# -*- coding: utf-8 -*-


"""
this program request from google maps the route form start to end point
return the route 
#pip install -U googlemaps
if you have SSL problem install
#pip install pyopenssl ndg-httpsclient pyasn1
example to use  python gmaps_test.py  start_location end_location
python gmaps_test.py -s "-22.819242,-47.0641516" -e "-22.82287,-47.0623451" -m "driving"
python gmaps_test.py -s "-22.8229406,-47.0622162" -e "-22.8193,-47.0640474"
Created on Mon Oct 29 23:19:30 2015
@author: olmerg
copyright 2015  Olmer Garcia  olmerg@gmail.com
Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

"""


import time
import googlemaps 
import sys, getopt
from geodesy import utm
import vilma_splines


if __name__ == '__main__':
	total = len(sys.argv)
	_mode="driving"
	start="-22.819242,-47.0641516"
	end="-22.82287,-47.0623451"
	if total>1:
		myopts, args = getopt.getopt(sys.argv[1:],"s:e:m:")
		for o, a in myopts:
    			if o == '-s':
    			    start=a
    			elif o == '-e':
    			    end=a
    			elif o == '-m':
    			    _mode=a
    			else:
    			    print("Usage: %s -s \"lat,lng\" -e \"lat,lng\"" % sys.argv[0])



	key = 'AIzaSyCVKpmqv9wDiC7RCohca6U9Fq5-kRLIVy0'
	client = googlemaps.Client(key)

	routes = client.directions(start, end,mode=_mode)


	#print routes

	#polyl=routes[0]['overview_polyline']['points']
	#points = googlemaps.convert.decode_polyline(polyl)
	#for x in points:
	#	print  str(x['lat'])+','+str(x['lng']) 
	#print 'start',routes[0]['legs'][0]['start_location']['lat'],routes[0]['legs'][0]['start_location']['lng']
	#print 'end',routes[0]['legs'][0]['end_location']['lat'],routes[0]['legs'][0]['end_location']['lng']
	polyl=routes[0]['legs'][0]['steps'][0]['polyline']['points']
	points = googlemaps.convert.decode_polyline(polyl)
	X=[]
	Y=[]
	for x in points:
		utm_data=utm.fromLatLong(x['lat'],x['lng'])
		X.append(utm_data.northing)
		Y.append(utm_data.easting)
		print  str(x['lat'])+','+str(x['lng']) 

	s,global_coeff,arclen=vilma_splines.arc_length_spline(X,Y,0,0)
	X1,Y1=vilma_splines.sToPlot(global_coeff,s,arclen,200)
	import matplotlib as mpl
	mpl.use("TkAgg")
	import matplotlib.pylab as plt
	plt.figure()
	plt.plot(X,Y,'ro')
	plt.plot(X1,Y1,'g')
	plt.show()
    




#print routes[0]['waypoint_order']['start_location'],routes[0]['end_location']



#polyl=routes[0]['overview_polyline']['points']
#points = googlemaps.convert.decode_polyline(polyl)
#print points

