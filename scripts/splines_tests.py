

import numpy as np
from scipy.integrate import quad
import vilma_splines 


 
 
if '__main__' in __name__:
    x = lambda n: np.linspace(-1,1,n)
    f = lambda x: np.cos(np.sin(np.pi*x))
    n = 5
    E=200
    data = zip(x(n),f(x(n)))
    s,global_coeff,arclen=vilma_splines.arc_length_spline(x(n),f(x(n)),0,0)
    print global_coeff
    print "-----------------"
    X1,Y1=vilma_splines.sToPlot(global_coeff,s,arclen,E)
    splines,xn = vilma_splines.Splines(x(n),f(x(n)),0,0)
    print splines
    
    X,Y = vilma_splines.splinesToPlot(splines,x(n),xn,E)
    import matplotlib as mpl
    mpl.use("TkAgg")
    import matplotlib.pylab as plt
    plt.figure()
    plt.plot(X1,Y1,'g--')
    plt.plot(X,Y,'r--')
    plt.plot(x(300),f(x(300)),'k')
    plt.show()
