# -*- coding: utf-8 -*-
#!/usr/bin/python
# Filename: vilma_splines.py

"""
this functions implement splines .....
Created on Mon Oct 29 23:19:30 2015
@author: olmerg
copyright 2015  Olmer Garcia  olmerg@gmail.com
Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

"""
import numpy as np
from scipy.integrate import quad


def segkernel(s,polyx,polyy):
	return (polyx[0]*s*s+polyx[1]*s+polyx[2])**2+(polyy[0]*s*s+polyy[1]*s+polyy[2])**2
	

def arc_length_spline(X,Y,angle_s,angle_e):
	n=len(X)
	if((abs(angle_s)-np.pi/2)<1e-3):
		mu_0=[0,np.sign(angle_s)]
	else:    
		mu_0=[1,1*tan(angle_s)]

	if((abs(angle_e)-np.pi/2)<1e-3):
		mu_n=[0,np.sign(angle_e)]
	else:   
		mu_n=[1,1*tan(angle_e)]
	dx=np.diff(X)
	dy=np.diff(Y)
	chordlen=np.sqrt(np.multiply(dx,dx)+np.multiply(dy,dy))
	arclen=sum(chordlen)
	diffarray=np.array([[3,0,0],[0,2,0],[0,0,1],[0,0,0]])
	s=np.concatenate(([0],np.cumsum(chordlen)))
	#print chordlen,arclen
	#print diffarray
	#for i xrange(0,2):
	coeff,ar=Splines(s,X,mu_0[0],mu_n[0])
	coefficientsx=np.dot(coeff,diffarray)
	coeff,ar=Splines(s,Y,mu_0[1],mu_n[1])
	coefficientsy=np.dot(coeff,diffarray)
	for i in xrange(0,n-1):
		I = quad(segkernel, 0, chordlen[i], args=(coefficientsx[i],coefficientsy[i]))
		#print I
		chordlen[i]=I[0]
	arclen=sum(chordlen)
	#print chordlen,arclen
	s=np.concatenate(([0],np.cumsum(chordlen)))
	coeffx,ar=Splines(s,X,mu_0[0],mu_n[0])
	coeffy,ar=Splines(s,Y,mu_0[1],mu_n[1])
	return s,np.concatenate((coeffx,coeffy),axis=1),arclen
	
 
def Splines_clamp(X,Y,dxdy_0,dxdy_n):
#  clamped spline http://www.personal.psu.edu/jjb23/web/htmls/sl455SP12/ch3/CH03_5C.pdf
#code iin python based in natural spline https://jayemmcee.wordpress.com/cubic-splines/
    np1=len(X)
    n=np1-1
    a = Y[:]
    b = [0.0]*(n)

    d = [0.0]*(n)
    h = np.diff(X)  #[X[i+1]-X[i] for i in xrange(n)]
    alpha = [0.0]*np1
    alpha[0]=3*(a[1]-a[0])/h[0]-3*dxdy_0;
    for i in xrange(1,n):
        alpha[i] = 3/h[i]*(a[i+1]-a[i]) - 3/h[i-1]*(a[i]-a[i-1])
    alpha[n]=3*dxdy_n-3*(a[n]-a[n-1])/h[n-1];
    
    c = [0.0]*np1
    L = [0.0]*np1
    u = [0.0]*np1
    z = [0.0]*np1
    L[0] = 2.0*h[0]
    u[0] = 0.5 
    z[0] = alpha[0]/L[0]
    for i in xrange(1,n):
        L[i] = 2*(X[i+1]-X[i-1]) - h[i-1]*u[i-1]
        u[i] = h[i]/L[i]
        z[i] = (alpha[i]-h[i-1]*z[i-1])/L[i]
    
    c[n] = (alpha[n]-h[n-1]*z[n-1])/(h[n-1]*(2-u[n-1]))

    for j in xrange(n-1, -1, -1):
        c[j] = z[j] - u[j]*c[j+1]
#	if j==n-1:
		
        b[j] = (a[j+1]-a[j])/h[j] - (h[j]*(c[j+1]+2*c[j]))/3
        d[j] = (c[j+1]-c[j])/(3*h[j])
    splines = np.transpose(np.concatenate((d,c[0:n],b,a[0:n])).reshape(4,n))
    #print splines
    return splines,X[n]
def Splines(X,Y,dxdy_0,dxdy_n):
#code based in natural spline https://jayemmcee.wordpress.com/cubic-splines/
    np1=len(X)
    n=np1-1
    a = Y[:]
    b = [0.0]*(n)
    d = [0.0]*(n)
    h = [X[i+1]-X[i] for i in xrange(n)]
    alpha = [0.0]*n
    for i in xrange(1,n):
        alpha[i] = 3/h[i]*(a[i+1]-a[i]) - 3/h[i-1]*(a[i]-a[i-1])
    c = [0.0]*np1
    L = [0.0]*np1
    u = [0.0]*np1
    z = [0.0]*np1
    L[0] = 1.0; u[0] = z[0] = 0.0
    for i in xrange(1,n):
        L[i] = 2*(X[i+1]-X[i-1]) - h[i-1]*u[i-1]
        u[i] = h[i]/L[i]
        z[i] = (alpha[i]-h[i-1]*z[i-1])/L[i]
    L[n] = 1.0; z[n] = c[n] = 0.0
    for j in xrange(n-1, -1, -1):
        c[j] = z[j] - u[j]*c[j+1]
        b[j] = (a[j+1]-a[j])/h[j] - (h[j]*(c[j+1]+2*c[j]))/3
        d[j] = (c[j+1]-c[j])/(3*h[j])
    splines=np.transpose(np.concatenate((d,c[0:n],b,a[0:n])).reshape(4,n))
    return splines,X[n] 
def splinesToPlot(splines,xn,x_e,res):
    n=np.shape(splines)
    n=n[0]
 
    perSpline = int(res/n)
    if perSpline < 3: perSpline = 3
    X=[]
    Y=[]
    for i in xrange(n):
        x0 = xn[i]
	if i<n-1:
	        x1 = xn[i+1]
	else:
		x1=x_e
        x = np.linspace(x0,x1,perSpline)
        for xi in x:
            X.append(xi)
            h=(xi-x0)
            Y.append(splines[i][3]+splines[i][2]*h + splines[i][1]*h**2 + splines[i][0]*h**3)
   
    return X,Y

def sToPlot(splines,sn,arclen,res):
    n=np.shape(splines)
    n=n[0]
    #print n
    perSpline = int(res/n)
    if perSpline < 3: perSpline = 3
    X=[]
    Y=[]
    for i in xrange(n):
        s0 = sn[i]
	if i<n-1:
	        s1 = sn[i+1]
	else:
		s1=arclen
        s = np.linspace(s0,s1,perSpline)
        for si in s:
	    h=(si-s0)
            X.append(splines[i][3]+splines[i][2]*h + splines[i][1]*h**2 + splines[i][0]*h**3)
            
            Y.append(splines[i][7]+splines[i][6]*h + splines[i][5]*h**2 + splines[i][4]*h**3)
   
    return X,Y


version = '0.1'

# End of  vilma_splines.py
